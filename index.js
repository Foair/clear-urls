import http from 'axios';
import { createHash } from 'node:crypto';
import { writeFile, mkdir } from 'node:fs/promises';

// https://docs.clearurls.xyz/
// https://github.com/ClearURLs/Rules/blob/master/data.min.json

const url = 'https://rules2.clearurls.xyz/data.minify.json';

const { data } = await http.get(url);

data.providers['bilibili.com'].rules.push('vd_source', 'from_spmid');

await mkdir('build');

const json = JSON.stringify(data);
await writeFile('build/rules.json', json, 'utf8');

const sha256sum = createHash('sha256');
sha256sum.update(json);
const sha256 = sha256sum.digest('hex');

await writeFile('build/rules.json.sha256', sha256, 'utf8');
console.log(`done`);
